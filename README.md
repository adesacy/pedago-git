# Formation Git & GitLab pour les SHS

Ce dépôt s'inscrit dans le cadre la formation dédiée à Git et GitLab organisée par l'URFIST et spécifiquement pensée pour les communautés en sciences humaines et sociales (SHS). Alors que ces outils sont traditionnellement associés au développement informatique, ils offrent des avantages considérables pour les projets collaboratifs, quel que soit le domaine. Qu'il s'agisse de projets de recherche, d’édition collaborative ou encore de la gestion de versions pour des bases de données, Git et GitLab peuvent répondre à certains besoins des équipes de recherche SHS.

Nous débuterons cette session par une introduction aux fondamentaux de Git, ses concepts et fonctionnalités clefs, avant de faire un tour d’horizon de son articulation avec des plateformes comme GitLab. Ensuite, nous plongerons dans les spécificités de l’utilisation de Git et GitLab pour les SHS, en mettant l’accent sur la gestion des versions et la collaboration, deux aspects cruciaux pour les chercheurs et gestionnaires de projets académiques.

À travers cette formation, vous apprendrez à :

- Installer et configurer Git et GitLab,
- Créer et gérer des dépôts pour organiser vos projets,
- Maîtriser les commandes de base et avancées pour suivre l’évolution de vos travaux,

Dans une seconde séance :

- Utiliser les fonctionnalités collaboratives de GitLab pour faciliter le travail en équipe,
- Exploiter le potentiel du CI/CD et des GitPages pour la création de sites web ou de wikis.

L’objectif est de fournir les compétences nécessaires pour tirer parti de ces outils dans des projets en SHS, tout en offrant des pratiques concrètes et adaptées aux besoins spécifiques.

Toute contribution ou remarque sot bien évidemment les bienvenues : voir sur ce point la section _Contribution_.

## Présentation et objectifs

- Présentation
- Objectif


## Pré-requis

- Notions de base en markdown.
- Utilisation sommaire d'un terminal.
- Installation de git.

## Plan de la formation

__1. Introduction__

1.1. Historique

1.2. Définition

1.3. Git VS GitLab, Github, etc.

1.4. Pourquoi utiliser Git ?

__2. Git pour les SHS__

2.1. Gestion de développement informatique avec Git et GitLab :

- Commandes de base :
    - Installation.
    - Création d’un dépôt.
    - Création d’un token ou d’une clef (HTTPs et SSH)
    - Synchronisation d’un dépôt. 
    - Premier commit, premier push.
	- _git add_, _git commit -m "...",  _git push_.
    - README.md.
    - Statut du dépôt.
    - .gitignore file.
    - Retour à un commit antérieur.
    - Conflits et gestion des conflits.

- G(U)It : GitHub Desktop, Git GUI...

2.2. Gestion de projets informatique avec GitLab

- Environnement GitLab : 
    - Qu’est-ce que GitLab ?
    - Git vs GitLab.
    - Gestion de projets et fonctionnalités.
        - Membre d’une équipe.
        - Issues.
            - Labels.
        - Issues Board.
        - Milestones.

- Commandes avancées pour travail collaboratif.
    - Récupération d'un état actuel d'un dépôt.
    - Création d’une branche.
    - Commits sur la branche.
    - Demande de merge request.

__3. Création de sites webs, wiki en utilisant le CI/CD de GitLab et GitPages__

3.1. Wikis

3.2. Création de sites statiques

- Création de sites en markdown.
- Qu’est-ce que le markdown ?
- Rmd et bookdown.
- Jekyll, Hugo et le web statique.

## Pré-requis à la formation : 

- Création d'un Humanid.
- Demande d'accès au service GitLab (https://documentation.huma-num.fr/gitlab/).

## Contribution

Pour contribuer au projet, apporter des corrections, des commentaires, il existe plusieurs options. Par ordre de commodité, vous pouvez :

- Faire un fork, procéder à vos proposition de modifications et proposer une merge request sur le dépôt GitLab.
- Ouvrir une issue détaillant votre contribution. Indiquez clairement où se trouve le texte, dans l'idéal grâce à une url spécifiant la ligne du fichier correspondant. Ajouter un label correspondant à la nature de votre modification (*bug*, *amélioration*, *orthographe*, *référence*, etc.). 
- Envoyez votre contribution par courriel à antoinedesacy( a t )gmail.com.

## Références

- https://git-scm.com.
- 


